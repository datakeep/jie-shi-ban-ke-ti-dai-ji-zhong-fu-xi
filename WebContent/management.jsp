<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理画面</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
        <div class="errorMessages">
            <ul>
                <c:forEach items="${errorMessages}" var="errorMessage">
                    <li><c:out value="${errorMessage}" />
                </c:forEach>
            </ul>
        </div>
        <c:remove var="errorMessages" scope="session" />
    </c:if>

	<div class="header">
		<!-- hrefにはURL書く(jspではない) -->
		<a href="./">ホーム</a>
	    <a href="signup">ユーザー新規登録</a><br /><br /><br/>
	</div>

	<!-- このドルカッコ内のmanagementsはDaoからService、
         Servletへ送られてきたUserBranchDepartmentの情報が詰められているもの -->
    <!-- Servletのmanagementsの内容をここではmanagementとして扱うよ -->

    <c:forEach items="${managements}" var="management">
        <div class="management">
              アカウント：<div class="account"><c:out value="${management.account}" /></div><br />
              名前：<div class="name"><c:out value="${management.name}" /></div><br />
              支社：<div class="branchName"><c:out value="${management.branchName}" /></div><br />
              部署：<div class="departmentName"><c:out value="${management.departmentName}" /></div><br />
              アカウント復活停止状態：<div class="isStopped"><c:out value="${management.isStopped}" /></div>

              <form action="setting" method="get"><br />
              	 <!-- 編集ボタン押されたと同時にuserIdという変数名でuserのidを送る -->
              	 <input type="hidden" name="userId" value="${management.id}">
           	  	 <input type="submit" value="編集"><br /><br />
           	  </form>

           	  <!-- Daoでuserのidをキーに値を入れていくのでuserのidも送る -->
       	  	  <form action="stop" method="post">
	           	  <!-- 停止ボタン押されたと同時にstopという変数に1とuserのIDを詰めて送る -->
	           	  <c:if test="${ management.isStopped==0 }">
	           	  	 <input type="hidden" name="userId" value="${management.id}">
	           	  	 <input type="hidden" name="stopped" value="1">
	           	  	 <input type="submit" value="停止">
	           	  </c:if>

	        	  <!-- 復活ボタン押されたと同時にstartという変数に0とuserのIDを詰めて送る -->
	        	  <c:if test="${ management.isStopped==1 }">
	        	  	 <input type="hidden" name="userId" value="${management.id}">
	           	  	 <input type="hidden" name="stopped" value="0">
	        	  	 <input type="submit" value="復活"><br /><br /><br />
	        	  </c:if>
       	  	  </form><br /><br /><br />
        </div>
    </c:forEach>

	<div class="copyright"> Copyright(c)SaitoKana</div>
</body>
</html>
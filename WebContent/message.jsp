<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿画面</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
	    <div class="errorMessages">
	        <ul>
	            <c:forEach items="${errorMessages}" var="errorMessage">
	                <li><c:out value="${errorMessage}" />
	            </c:forEach>
	        </ul>
	    </div>
    	<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="form-area">
        <form action="message" method="post">
        	<a href="./">ホーム</a><br />
            投稿してちょ<br /><br />

			<!-- value=ドルで入力内容保持 -->
            <label for="title">件名</label>
            <input name="title" id="title" value="${title}"/> <br />

            <label for="category">カテゴリ</label>
            <input name="category" id="category" value="${category}"/> <br />

			<!-- textereaはvalue使えないから<>の外に書く -->
            <label for="text">投稿内容</label>
            <textarea name="text" cols="100" rows="5" class="tweet-box" >${text}</textarea><br />

            <input type="submit" value="投稿">
        </form>
	</div>
	<div class="copylight"> Copyright(c)SaitoKana</div>
</body>
</html>
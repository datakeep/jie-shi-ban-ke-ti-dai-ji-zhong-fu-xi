<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー編集画面</title>
</head>
 <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                    <c:remove var="errorMessages" scope="session" />
                </div>
            </c:if>

			<form action="setting" method="post"><br />

				<!-- ここはURLに対応 -->
				<a href="management">ユーザー管理</a> <br /><br />

	            <label for="account">アカウント</label>
	            <input name="account" value="${user.account}" id="account" /> <br />

	   			<!-- type="password"で●化 -->
	            <label for="password">パスワード</label>
	            <input name="password" type="password" id="password" /> <br />

	            <label for="confirmedPassword">確認用パスワード</label>
	            <input name="confirmedPassword" type="password" id="confirmedPassword" /> <br />

	            <label for="name">名前</label>
	            <input name="name" value="${user.name}" id="name" /> <br /><br />

				<!-- タグの間に入ってるのは表示するだけの支社名と部署名
				     実際はbranchIdとdepartmentIdをServletに送っている -->

				<!-- ログインしてる人と変更しようとしてる人が違うとき -->
				<c:if test="${loginUser.id != user.id}">
					<label for ="branches">支社</label><br />
				   	<!-- selectタグでプルダウンメニュー作る -->
	               	<select size="1" name="branchId" id="branchId">
		               <c:forEach items="${branches}" var="branch">

		               		<!--支社IDとユーザーの支社IDが一致したら選択肢の中に支社IDを表示しておく-->
	               			<c:if test="${branch.id == user.branchId}">

	               				<!-- optionタグはプルダウンの中身に表示される項目 -->
	               				<!-- valueは選択項目を送信する際に受けとり手に値を知らせる -->
	               				<option value="${branch.id}" selected> ${branch.name}</option>
	               			</c:if>
	               			<!--支社IDとユーザーの支社IDが一致しなかったら選択式-->
	               			<c:if test="${branch.id != user.branchId}">
	               				<option value="${branch.id}"> ${branch.name}</option><br />
	               			</c:if>

			   		   	</c:forEach>
			   	   	</select><br /><br />
			   	</c:if>

			   	<!--ログインしている人と変更しようとしているユーザーが一緒の場合支社が変更できない-->
               	<c:if test="${loginUser.id == user.id}">
               		<label for ="branches">支社</label><br />
	               	<select name="branchId">
	               		<c:forEach items="${branches}" var="branch">
	               			<c:if test="${branch.id == user.branchId}">
	               				<option value="${branch.id}" selected> ${branch.name}</option><br />
	               			</c:if>
	               		</c:forEach>
	               	</select><br /><br />
	            </c:if>

				<!-- ログインしてる人と変更しようとしてる人が違うとき -->
				<c:if test="${ loginUser.id != user.id }">
					<label for ="departments">部署</label><br />
				   	<select size="1" name="departmentId" id="departmentId">

				   	   <!-- このドルカッコ内はServletと対応 -->
				   	   <c:forEach items="${departments}" var="department">

				   	   		<!-- このドルカッコ内はbeansと対応 -->
				   	   		<!--部署IDとユーザーの部署IDが一致したら選択肢の中に部署名を表示しておく-->
	               			<c:if test="${department.id == user.departmentId}">
	               				<option value="${department.id}" selected> ${department.name}</option>
	               			</c:if>
	               			<!--部署IDとユーザーの部署IDが一致しなかったら選択式-->
							<c:if test="${department.id != user.departmentId}">
               					<option value = "${department.id}">${department.name}</option>
               				</c:if>
		               	</c:forEach>
		           	</select><br /><br />
		        </c:if>

		        <!--ログインしている人と変更しようとしているユーザーが一緒の場合部署が変更できない-->
                <c:if test="${loginUser.id == user.id}">
                	<label for ="departments">部署</label><br />
                	<select name="departmentId">
                		<c:forEach items="${departments}" var="department">
                			<c:if test="${department.id == user.departmentId}">
                				<option selected value = "${department.id}">${department.name}</option>
                			</c:if>
                		</c:forEach>
                	</select><br /><br />
                </c:if>

				<!-- Servletにuserの中にあるidを飛ばす -->
				<input name="id" value="${user.id}" id="id" type="hidden"/>
	            <input type="submit" value="更新" /> <br />
			</form>
			<div class="copyright"> Copyright(c)SaitoKana</div>
     	</div>

    </body>
</html>
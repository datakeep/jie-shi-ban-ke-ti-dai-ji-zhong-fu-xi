package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import beans.UserBranchDepartment;
import dao.UserBranchDepartmentDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	//新規登録画面で入力された情報をDaoへ
	public void insert(User user) {
        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//ログイン画面で入力された情報をUserDaoへ
	public User select(String account, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, account, encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//Daoでデータベースから取得した内容を受け取ってManagementServletに渡す
    public List<UserBranchDepartment> select() {
    	final int LIMIT_NUM = 1000;//100件まで取得
        Connection connection = null;
        try {
            connection = getConnection();
            List<UserBranchDepartment> managements = new UserBranchDepartmentDao().select(connection, LIMIT_NUM);
            commit(connection);

            return managements;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザー管理画面にログインユーザーの情報を保持＆表示のための処理
    public User select(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザー編集画面で更新ボタン押された後動く処理(update用)
    public void update(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().update(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザー復活停止用にjspから持ってきた0と1をDaoへ
  	public void insert(int stopped, int userId) {
          Connection connection = null;
          try {
              connection = getConnection();
              new UserDao().insert(connection, stopped, userId);
              commit(connection);
          } catch (RuntimeException e) {
              rollback(connection);
              throw e;
          } catch (Error e) {
              rollback(connection);
              throw e;
          } finally {
              close(connection);
          }
      }
}

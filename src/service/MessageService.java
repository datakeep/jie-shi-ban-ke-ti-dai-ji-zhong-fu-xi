package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	//投稿画面で入力された内容をMessageDaoに受け渡し
    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //Daoでデータベースから取得した内容を受け取ってHomeServletに渡す
    public List<UserMessage> select(String start, String end, String category) {
    	final int LIMIT_NUM = 1000;//100件まで取得

        Connection connection = null;

        //現在日時の取得
        Date date = new Date();

        try {
            connection = getConnection();

            //startが入力されていたら
            if(!StringUtils.isBlank(start)){
            	start = start + " 00:00:00";
            } else {
            	//startが入力されていなかったらデフォルト値入れる
            	start = "2020-01-01 00:00:00";
            }

            //endが入力されていたら
            if(!(StringUtils.isBlank(end))) {
            	end = end + " 23:59:59";
            } else {
            	//endが入力されていなかったら現在時刻入れる
            	String sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);
            	end = sdf;
            }

            //カテゴリが入力されていたら
            if(!StringUtils.isBlank(category)){
            	category = "%" + category + "%";
            } else {
            	//カテゴリ入力されていなかったら
            	category = null;
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, start, end, category);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //投稿消去押された際に送られるmessageIdをDaoへ受け渡し
    public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;

public class CommentService {

	//コメント投稿ボタン押された後コメントをデータベースに入れる処理
    public void insert(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentDao().insert(connection, comment);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //データベースから持ってきたコメントをServletに渡す
    public List<UserComment> select() {
    	final int LIMIT_NUM = 1000;//100件まで取得
        Connection connection = null;
        try {
            connection = getConnection();
            List<UserComment> comments = new UserCommentDao().select(connection, LIMIT_NUM);
            commit(connection);

            return comments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメント消去押された際に送られるcommentIdをDaoへ受け渡し
    public void delete(int commentId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentDao().delete(connection, commentId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
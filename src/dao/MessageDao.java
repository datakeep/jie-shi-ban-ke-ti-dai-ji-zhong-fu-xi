package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

  public void insert(Connection connection, Message message) {

	//投稿をデータベースに挿入
    PreparedStatement ps = null;
    try {
      StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    id, ");
            sql.append("    title, ");
            sql.append("    text, ");
            sql.append("    category, ");
            sql.append("    user_id, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");                                  // user_id
            sql.append("    ?, ");
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

      ps = connection.prepareStatement(sql.toString());

      ps.setInt(1, message.getId());
      ps.setString(2, message.getTitle());
      ps.setString(3, message.getText());
      ps.setString(4, message.getCategory());
      ps.setInt(5, message.getUserId());
      ps.executeUpdate();

    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }

  public void delete(Connection connection, int messageId) {

	//投稿をデータベースに挿入
    PreparedStatement ps = null;
    try {
      StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages WHERE id = ?");

      ps = connection.prepareStatement(sql.toString());
      ps.setInt(1, messageId);
      ps.executeUpdate();

    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }
}
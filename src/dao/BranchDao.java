package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branches;
import exception.SQLRuntimeException;

public class BranchDao {
	public List<Branches> select(Connection connection) {

		//データベースからbranchesテーブルの情報取り出し
        PreparedStatement ps = null;
        try {
             StringBuilder sql = new StringBuilder();
             sql.append("SELECT ");
             sql.append("    id, ");
             sql.append("    name, ");
             sql.append("    created_date, ");
             sql.append("    updated_date  ");
             sql.append("FROM branches ");

             ps = connection.prepareStatement(sql.toString());

             ResultSet rs = ps.executeQuery();

             List<Branches> branches = toBranches(rs);
             return branches;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	 }

	 //データベースから持ってきた後Branchesという名前のリストに詰める
	 private List<Branches> toBranches(ResultSet rs) throws SQLException {

	    List<Branches> branches = new ArrayList<Branches>();
	    try {
	        while (rs.next()) {
	            Branches branch = new Branches();
	            branch.setId(rs.getInt("id"));
                branch.setName(rs.getString("name"));
                branch.setCreatedDate(rs.getTimestamp("created_date"));
                branch.setUpdatedDate(rs.getTimestamp("updated_date"));

                branches.add(branch);
	        }
	        return branches;
	    } finally {
	        close(rs);
	    }
	}
}

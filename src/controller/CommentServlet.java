package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	//コメント投稿ボタン押された後の処理
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();

    	//jspと繋ぎ合わせ＆定義
        String text = request.getParameter("text");
        int messageId = Integer.parseInt(request.getParameter("messageId"));

        //**Comment型のcommentという名前の変数にtext,messageIdを詰める
        Comment comment = new Comment();
        comment.setText(text);
        comment.setMessageId(messageId);

        //request内からエラー表示に必要な情報取り出し
        //booleanで振り分け後のエラー処理をまとめてここに集約
        //commentはbooleanで使用するので入れてある
        if(!isValid(request, comment, errorMessages)) {
			session.setAttribute("text", text);
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        //User.javaからId(userId)取ってきて、変数commentに詰める
        User user = (User)session.getAttribute("loginUser");
        comment.setUserId(user.getId());

        //情報が詰まったcommentをCommentServiceに送る
        new CommentService().insert(comment);
        response.sendRedirect("./");
    }

    private boolean isValid(HttpServletRequest request, Comment comment, List<String> errorMessages) {

    	//**のcommentという変数から使うもの取り出し
        String text = comment.getText();

        //投稿内容
        if (StringUtils.isEmpty(text)) {
            errorMessages.add("コメントを入力してください");
        } else if (50 <= text.length()) {
            errorMessages.add("コメントは50文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
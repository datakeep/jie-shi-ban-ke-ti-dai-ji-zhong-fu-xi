package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	//新規投稿押されたときmessage.jspを表示
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

        request.getRequestDispatcher("message.jsp").forward(request, response);
    }

	//投稿ボタン押された時行われる処理
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//ServletではHttpSession定義で簡単にSessionを使うことができる
        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        //jspと繋ぎ合わせ＆定義
        String title = request.getParameter("title");
        String category = request.getParameter("category");
        String text = request.getParameter("text");

        //**投稿画面で入力された情報をmessageという変数に詰める
        Message message = new Message();
        message.setTitle(title);
        message.setCategory(category);
        message.setText(text);

        //request内からエラー表示に必要な情報取り出し
        //booleanで振り分け後のエラー処理をまとめてここに集約
        //messageはbooleanで使用するので入れてある
        if(!isValid(request, message, errorMessages)) {
        	session.setAttribute("title", title);
			session.setAttribute("category", category);
			session.setAttribute("text", text);
            session.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("message.jsp").forward(request, response);
            return;
        }

        //User.javaからId取ってきて、変数messageに詰める
        User user = (User)session.getAttribute("loginUser");
        message.setUserId(user.getId());

        //情報が詰まったmessage,userをServiceに送る
        new MessageService().insert(message);
        response.sendRedirect("./");
    }

    private boolean isValid(HttpServletRequest request, Message message, List<String> errorMessages) {

    	//**のmessageという変数から使うもの取り出し
    	String title = message.getTitle();
        String category = message.getCategory();
        String text = message.getText();

        //件名
        if (StringUtils.isEmpty(title)) {
            errorMessages.add("件名を入力してください");
        } else if (30 <= title.length()) {
            errorMessages.add("件名は30文字以下で入力してください");
        }

        //カテゴリ
        if (StringUtils.isEmpty(category)) {
            errorMessages.add("カテゴリを入力してください");
        } else if (10 <= category.length()) {
            errorMessages.add("カテゴリは10文字以下で入力してください");
        }

        //投稿内容
        if (StringUtils.isEmpty(text)) {
            errorMessages.add("本文を入力してください");
        } else if (100 <= text.length()) {
            errorMessages.add("本文は100文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}

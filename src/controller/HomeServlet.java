package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//home.jspの表示
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

	String start = request.getParameter("start");
	String end = request.getParameter("end");
	String category = request.getParameter("category");

	//データベースから取得した投稿情報をServiceから受け取る
	List<UserMessage> messages = new MessageService().select(start, end, category);

	//データベースから取得したコメントをServiceから受け取る
	List<UserComment> comments = new CommentService().select();

	//ダブルコーテーション内(青)はjspと対応
	request.setAttribute("start", start);
    request.setAttribute("end", end);
    request.setAttribute("category", category);
    request.setAttribute("messages", messages);
    request.setAttribute("comments", comments);
    request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}

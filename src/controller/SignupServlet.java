package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branches;
import beans.Departments;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	//signup.jsp表示
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//データベースからDaoで取り出した情報をServletへともってきたList(帰り復路)
    	List<Branches> branches = new BranchService().select();
        List<Departments> departments = new DepartmentService().select();
        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    //登録ボタン押された後の処理
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {


        //登録ボタン押されたときエラー表示に必要な情報が詰まったリストを持ってくる
    	List<Branches> branches = new BranchService().select();
        List<Departments> departments = new DepartmentService().select();
        List<String> errorMessages = new ArrayList<String>();

        User user = getUser(request);

        //登録ボタン押されたときエラー表示に必要な情報を取り出す
        if (!isValid(request, user, errorMessages)) {
        	request.setAttribute("user", user);
	    	request.setAttribute("branches", branches);
	    	request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
        new UserService().insert(user);
        response.sendRedirect("management");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	//新規登録画面で入力した情報をuserという変数に詰めてServiceへ
        User user = new User();
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setConfirmedPassword(request.getParameter("confirmedPassword"));
        user.setName(request.getParameter("name"));

        //jspのプルダウンで選択された情報をデータベースに持っていく(行き往路)
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(HttpServletRequest request, User user, List<String> errorMessages) {

    	String account = user.getAccount();
        String password = user.getPassword();
        String confirmedPassword = user.getPassword();
        String name = user.getName();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        //アカウント
        if (StringUtils.isEmpty(account)) {
        	//アカウントが空白だったら
            errorMessages.add("アカウント名を入力してください");
        } else if ((account.length() > 10))  {
        	//20文字以上だったら
            errorMessages.add("アカウント名は10文字以下で入力してください");
        }

        //パスワード
        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }

        //確認用パスワード
        if (StringUtils.isEmpty(confirmedPassword)) {
            errorMessages.add("確認用パスワードを入力してください");
        }

        //パスワードの一致確認
        if(!password.equals(confirmedPassword)) {
        	errorMessages.add("パスワードが一致しません");
        }
        //文字列はイコール使えない
        /*if (password != confirmedPassword) {
            errorMessages.add("パスワードが一致しません");
        }*/

        //名前
        if (StringUtils.isEmpty(name)) {
        	//名前が空白だったら
            errorMessages.add("名前を入力してください");
        } else if (name.length() >= 10) {
        	//名前が10文字以上だったら
        	errorMessages.add("名前は10文字以下で入力してください");
        }

        //支社と部署の組み合わせ
        if ((branchId == 1) && ((departmentId == 2) || (departmentId == 3) || (departmentId == 4))){
        	//支社が1のとき、部署が1じゃなかったら、
            errorMessages.add("支社と部署の組み合わせが不正です");
        }
        if ((departmentId == 1) && ((branchId == 2) || (branchId == 3) || (branchId == 4))) {
        	//部署が1で、支社が1じゃなかったら
        	errorMessages.add("支社と部署の組み合わせが不正です");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}


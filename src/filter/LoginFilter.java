package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//これから使う変数たちを定義
        String requestPath = ((HttpServletRequest)request).getServletPath();
        List<String> errorMessages = new ArrayList<String>();

		//呼び出しがloginじゃないとき
		if(!requestPath.equals("/login")){
			//"loginUser"という名前のsessionに詰めていた情報をloginUserという新しい変数に入れ込む
			HttpSession session = ((HttpServletRequest)request).getSession();
			User loginUser = (User) session.getAttribute("loginUser");

			//loginUserがいないとき
			if (loginUser == null) {
		        //セッションがNullならば、ログイン画面へ飛ばす
				errorMessages.add("ログインしてください");
				session.setAttribute("errorMessages", errorMessages);
				((HttpServletResponse)response).sendRedirect("./login");
				return;
		    }
		}
		// サーブレットを実行
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
